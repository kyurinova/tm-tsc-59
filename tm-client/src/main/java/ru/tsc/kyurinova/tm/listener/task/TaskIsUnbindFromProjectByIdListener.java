package ru.tsc.kyurinova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.endpoint.TaskDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class TaskIsUnbindFromProjectByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-unbind-from-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    @EventListener(condition = "@taskIsUnbindFromProjectByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final SessionDTO session = sessionService.getSession();
        ;
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findByIdProject(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = taskEndpoint.findByIdTask(session, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        projectTaskEndpoint.unbindTaskById(session, projectId, taskId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
