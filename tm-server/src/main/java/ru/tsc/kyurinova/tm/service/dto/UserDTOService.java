package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.dto.IUserDTOService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.user.EmailExistsException;
import ru.tsc.kyurinova.tm.exception.user.LoginExistsException;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class UserDTOService extends AbstractDTOService implements IUserDTOService {

    @NotNull
    @Autowired
    public IUserDTORepository userRepository;

    @Autowired
    private IPropertyService propertyService;

    @Override
    @Transactional
    public void addAll(@NotNull final List<UserDTO> users) {
        for (UserDTO user : users) {
            userRepository.add(user);
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable UserDTO user = userRepository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable UserDTO user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    public void isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final UserDTO user = userRepository.findByLogin(login);
        if (user != null) throw new LoginExistsException();
    }

    @Override
    public void isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserDTO user = userRepository.findByEmail(email);
        if (user != null) throw new EmailExistsException();
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO createAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.ADMIN);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        isLoginExists(login);
        isEmailExists(email);
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        isLoginExists(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new EmptyUserIdException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        userRepository.update(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.update(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("Y");
        userRepository.update(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("N");
        userRepository.update(user);
        return user;
    }

    @Override
    @Transactional
    public void remove(@Nullable final UserDTO entity) {
        if (entity == null) throw new EntityNotFoundException();
        userRepository.removeById(entity.getId());
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        userRepository.clear();
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @NotNull
    @Override
    public UserDTO findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return userRepository.findByIndex(index);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final UserDTO user = findById(id);
        if (user != null) {
            userRepository.removeById(id);
        }
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable final UserDTO user = findByIndex(index);
        if (user != null) {
            userRepository.removeByIndex(index);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return (userRepository.findById(id) != null);
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        return (userRepository.findByIndex(index) != null);
    }

    @Override
    public int getSize() {
        return userRepository.getSize();
    }
}
