package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.model.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.model.ITaskRepository;
import ru.tsc.kyurinova.tm.api.service.model.IProjectTaskService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectTaskService extends AbstractService implements IProjectTaskService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @NotNull
    @Autowired
    public ITaskRepository taskRepository;

    @Override
    @Transactional
    public void bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Transactional
    public void unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Transactional
    public void removeAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

}
