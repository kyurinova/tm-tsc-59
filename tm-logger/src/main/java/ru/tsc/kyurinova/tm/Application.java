package ru.tsc.kyurinova.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.kyurinova.tm.component.Bootstrap;
import ru.tsc.kyurinova.tm.configuration.LoggerConfiguration;

public class Application {

    @SneakyThrows
    public static void main(@NotNull String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
